function getLogin() {
  const username = document.getElementById('logUser').value;
  const password = document.getElementById('logPass').value;
  return { username, password };
}

async function dataSend() {
  const logData = getLogin();
  const response = await fetch('http://localhost:8000/login', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(logData),
  });

  const { status } = await response.json();
  const formLogin = document.getElementById('formLogin');

  switch (status) {
    case 'success': {
      // (function () {
      //   // const closeModal = document.getElementById('#buttonClose');
      //   'submitButton'.click(function () {
      //     'logModal'.modal('hide');
      //   });
      //   // closeModal.click();
      // });
      let hideModal = document.getElementById('buttonClose');
      hideModal.click();
      document.getElementById('loginBtn').classList.add('d-none');
      document.getElementById('registBtn').classList.add('d-none');
      document.getElementById('userBtn').classList.remove('d-none');
      document.getElementById('userDsbrd').classList.remove('d-none');
      break;
    }
    case 'wrong-password': {
      formLogin.classList.add('was-validated');
      const password = document.getElementById('logPass');
      password.classList.add('is-invalid');
      password.setCustomValidity('Wrong password!');
      break;
    }
    case 'no-username': {
      formLogin.classList.add('was-validated');
      const username = document.getElementById('logUser');
      username.classList.add('is-invalid');
      username.setCustomValidity('Username not found!');
      break;
    }
  }
}
