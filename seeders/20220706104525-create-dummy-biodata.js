'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'UserBiodata',
      [
        {
          name: 'Alpha One',
          company: 'IBM',
          description: 'Have Fun',
          user_game_id: 1,
          createdAt: '2022-07-05 20:18:47.517+07',
          updatedAt: '2022-07-05 20:18:47.517+07',
        },
        {
          name: 'Admin Nine',
          company: 'Google',
          description: 'Busy',
          user_game_id: 2,
          createdAt: '2022-07-05 20:18:47.517+07',
          updatedAt: '2022-07-05 20:18:47.517+07',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
