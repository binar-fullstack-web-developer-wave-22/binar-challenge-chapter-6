'use strict';

const { sequelize } = require('../models');

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'UserHistories',
      [
        {
          gameTitle: 'RPS',
          score: 10,
          time: 10.0,
          user_game_id: 1,
          createdAt: '2022-07-05 20:18:47.517+07',
          updatedAt: '2022-07-05 20:18:47.517+07',
        },
        {
          gameTitle: 'Solitaire',
          score: 50,
          time: 12.0,
          user_game_id: 2,
          createdAt: '2022-07-05 20:18:47.517+07',
          updatedAt: '2022-07-05 20:18:47.517+07',
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    //   const Op = Sequelize.Op;
    //   await queryInterface.bulkDelete('UserHistories', { user_game_id: { [Op.in]: [1, 2] } }, {});
  },
};
