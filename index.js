const express = require('express');
const app = express();
const port = 8000;
const fs = require('fs');
let userId = require('./id-pass.json');
let dataRev = require('./data-review.json');
const { UserGame, UserBiodata, UserHistory } = require('./models');

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.static('node_modules'));
app.use(express.static(`${__dirname}/views`));
app.use(express.json());

app.get('/', (request, response) => {
  response.render('ch3', {
    name: userId[0].name,
    data: dataRev,
  });
});

app.get('/game', (request, response) => {
  response.render('ch4', {
    title: 'ROCK PAPER SCISSOR',
    displayId: true,
    viewOnly: false,
  });
});

app.post('/login', (request, response) => {
  const { username, password } = request.body;
  const user = userId.find((user) => user.username === username);

  if (user) {
    if (user.password === password) {
      response.status(200).json({ status: 'success', ...user });
    } else {
      response.status(403).json({ status: 'wrong-password' });
    }
  } else {
    response.status(404).json({ status: 'no-username' });
  }
});

// CH6

// Dashboard GET all
app.get('/user-dashboard/create', (req, res) => {
  res.render('userCreate');
});

app.get('/biodata/create', (req, res) => {
  res.render('bioCreate');
});

app.get('/user-dashboard', async (req, res) => {
  const users = await UserGame.findAll();
  const bio = await UserBiodata.findAll();
  const hist = await UserHistory.findAll();
  res.render('dashboard', { users, bio, hist });
});

// CREATE user
app.post('/user-dashboard/create', async (req, res) => {
  const { username, password } = req.body;
  UserGame.create({ username, password })
    .then((UserGame) => {
      res.status(200).redirect('/user');
    })
    .catch((err) => {
      res.status(422).json('Failed to create an user');
    });
});

app.post('/biodata/create', (req, res) => {
  const { name, company, description, user_game_id } = req.body;
  UserBiodata.create(
    { name, company, description, user_game_id },
    {
      where: {
        id: req.params.id,
      },
    }
  )
    .then((UserBiodata) => {
      res.status(200).redirect('/user');
    })
    .catch((err) => {
      res.status(422).json('Failed to create a Biodata');
    });
});

//READ user by ID
app.get('/user/:id', (req, res) => {
  UserGame.findOne({
    where: { id: req.params.id },
  }).then((UserGame) => {
    res.status(200).json(UserGame);
  });
});

app.get('/biodata/:id', (req, res) => {
  const { name, company, description, user_game_id } = req.body;
  UserBiodata.findOne(
    { name, company, description, user_game_id },
    {
      where: { id: req.params.id },
    }
  )
    .then((UserBiodata) => {
      res.status(200).json(UserBiodata);
    })
    .catch((err) => {
      res.status(422).json('Failed to update an user');
    });
});

// UPDATE user
app.post('/user-update/:id', (req, res) => {
  const { username, password } = req.body;
  UserGame.update(
    { username, password },
    {
      where: { id: req.params.id },
    }
  )
    .then((UserGame) => {
      res.status(200).json(UserGame);
      res.render('update1');
    })
    .catch((err) => {
      res.status(422).json('Failed to update an user');
    });
});

app.post('/biodata-update/:id', (req, res) => {
  const { name, company, description, user_game_id } = req.body;
  UserBiodata.update(
    { name, company, description, user_game_id },
    {
      where: { id: req.params.id },
    }
  )
    .then((UserBiodata) => {
      res.status(200).json(UserBiodata);
      res.render('update2');
    })
    .catch((err) => {
      res.status(422).json('Failed to update an user');
    });
});

// DELETE
app.get('/user-dashboard/delete/:id', (req, res) => {
  UserGame.destroy({
    where: { id: req.params.id },
  });
  UserBiodata.destroy({
    where: { id: req.params.id },
  });
  res.status(200).redirect('/user-dashboard');
});

app.listen(port, () => {
  console.log('Server constructed!');
});
